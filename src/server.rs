use std::env;
use std::error::Error as StdError;
use std::fmt;
use std::fs;
use std::io::Write as IOWrite;
use std::net::Ipv4Addr;
use std::sync::mpsc::{Receiver, Sender};

use data_url::DataUrl;

use iron::modifiers::Redirect;
use iron::prelude::*;
use iron::{
    headers, status, typemap, AfterMiddleware, Iron, IronError, IronResult, Request, Response, Url,
};
use iron_cors::CorsMiddleware;
use mount::Mount;
use params::{FromValue, Params, Value as ParamsValue};
use path::PathBuf;
use persistent::Write;
use router::Router;
use serde_json;
use staticfile::Static;

use config::DEFAULT_UPLOADS_DIRECTORY;
use errors::*;
use exit::{exit, ExitResult};
use network::{AccessPointCredentials, NetworkCommand, NetworkCommandResponse};

struct RequestSharedState {
    gateway: Ipv4Addr,
    server_rx: Receiver<NetworkCommandResponse>,
    network_tx: Sender<NetworkCommand>,
    exit_tx: Sender<ExitResult>,
}

impl typemap::Key for RequestSharedState {
    type Value = RequestSharedState;
}

#[derive(Debug)]
struct StringError(String);

impl fmt::Display for StringError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl StdError for StringError {
    fn description(&self) -> &str {
        &*self.0
    }
}

macro_rules! get_request_ref {
    ($req:ident, $ty:ty, $err:expr) => {
        match $req.get_ref::<$ty>() {
            Ok(val) => val,
            Err(err) => {
                error!($err);
                return Err(IronError::new(err, status::InternalServerError));
            }
        }
    };
}

macro_rules! get_param {
    ($params:ident, $param:expr, $ty:ty) => {
        match $params.get($param) {
            Some(value) => match <$ty as FromValue>::from_value(value) {
                Some(converted) => converted,
                None => {
                    let err = format!("Unexpected type for '{}'", $param);
                    error!("{}", err);
                    return Err(IronError::new(StringError(err), status::BadRequest));
                }
            },
            None => {
                let err = format!("'{}' not found in request params: {:?}", $param, $params);
                error!("{}", err);
                return Err(IronError::new(StringError(err), status::BadRequest));
            }
        }
    };
}

macro_rules! get_optional_param {
    ($params:ident, $param:expr, $ty:ty) => {
        match $params
            .get($param)
            .map(|value| <$ty as FromValue>::from_value(value))
        {
            Some(Some(value)) => Some(value),
            Some(None) => {
                let err = format!("Unexpected type for '{}'", $param);
                error!("{}", err);
                return Err(IronError::new(StringError(err), status::BadRequest));
            }
            None => None,
        }
    };
}

macro_rules! get_request_state {
    ($req:ident) => {
        get_request_ref!(
            $req,
            Write<RequestSharedState>,
            "Getting reference to request shared state failed"
        )
        .as_ref()
        .lock()
        .unwrap()
    };
}

fn save_upload_field(
    data_url_string: &str,
    upload_dir: &PathBuf,
    suffix: &str,
    ext: &str,
) -> Result<String> {
    let mut upload_path = upload_dir.clone();
    upload_path.push(&format!("{}-{}.{}", uuid::Uuid::new_v4(), suffix, ext));
    let mut upload_file = fs::File::create(upload_path.clone())?;
    let url_data = DataUrl::process(data_url_string)
        .map_err(|e| format!("failed to process data-url: {:?}", e))?;
    let (body, _) = url_data
        .decode_to_vec()
        .map_err(|e| format!("failed to decode data-url: {:?}", e))?;
    upload_file.write_all(&body)?;
    if let Ok(string_path) = upload_path.into_os_string().into_string() {
        Ok(string_path)
    } else {
        Err("invalid upload path".into())
    }
}

macro_rules! get_optional_upload {
    ($params:ident, $field:expr, $uploads_directory:expr, $subdir:expr, $ext:expr, $upload:ident) => {
        match $params.get($field) {
            Some(ParamsValue::String(data_url_string)) => {
                let mut dir = $uploads_directory.clone();
                dir.push($subdir);
                if let Ok(upload_path) = save_upload_field(data_url_string, &dir, $field, $ext) {
                    $upload = Some(upload_path);
                } else {
                    let err = format!("Failed to process upload for '{}'", $field);
                    error!("{}", err);
                    return Err(IronError::new(
                        StringError(err),
                        status::InternalServerError,
                    ));
                }
            }
            _ => {}
        };
    };
}

macro_rules! get_upload {
    ($params:ident, $field:expr, $uploads_directory:expr, $subdir:expr, $ext:expr, $upload:ident) => {
        let mut optional_upload = None;
        get_optional_upload!(
            $params,
            $field,
            $uploads_directory,
            $subdir,
            $ext,
            optional_upload
        );

        match optional_upload {
            Some(upload_path) => $upload = upload_path.clone(),
            None => {
                let err = format!("{} not provided", $field);
                error!("{}", err);
                return Err(IronError::new(
                    StringError(err),
                    status::InternalServerError,
                ));
            }
        }
    };
}

fn exit_with_error<E>(state: &RequestSharedState, e: E, e_kind: ErrorKind) -> IronResult<Response>
where
    E: ::std::error::Error + Send + 'static,
{
    let description = e_kind.description().into();
    let err = Err::<Response, E>(e).chain_err(|| e_kind);
    exit(&state.exit_tx, err.unwrap_err());
    Err(IronError::new(
        StringError(description),
        status::InternalServerError,
    ))
}

struct RedirectMiddleware;

impl AfterMiddleware for RedirectMiddleware {
    fn catch(&self, req: &mut Request, err: IronError) -> IronResult<Response> {
        let gateway = {
            let request_state = get_request_state!(req);
            format!("{}", request_state.gateway)
        };

        if let Some(host) = req.headers.get::<headers::Host>() {
            if host.hostname != gateway {
                let url = Url::parse(&format!("http://{}/", gateway)).unwrap();
                return Ok(Response::with((status::Found, Redirect(url))));
            }
        }

        Err(err)
    }
}

pub fn start_server(
    gateway: Ipv4Addr,
    listening_port: u16,
    server_rx: Receiver<NetworkCommandResponse>,
    network_tx: Sender<NetworkCommand>,
    exit_tx: Sender<ExitResult>,
    ui_directory: &PathBuf,
) {
    let exit_tx_clone = exit_tx.clone();
    let gateway_clone = gateway;
    let request_state = RequestSharedState {
        gateway,
        server_rx,
        network_tx,
        exit_tx,
    };

    let mut router = Router::new();
    router.get("/", Static::new(ui_directory), "index");
    router.get("/networks", networks, "networks");
    router.post("/connect", connect, "connect");

    let mut assets = Mount::new();
    assets.mount("/", router);
    assets.mount("/static", Static::new(&ui_directory.join("static")));
    assets.mount("/css", Static::new(&ui_directory.join("css")));
    assets.mount("/img", Static::new(&ui_directory.join("img")));
    assets.mount("/js", Static::new(&ui_directory.join("js")));

    let cors_middleware = CorsMiddleware::with_allow_any();

    let mut chain = Chain::new(assets);
    chain.link(Write::<RequestSharedState>::both(request_state));
    chain.link_after(RedirectMiddleware);
    chain.link_around(cors_middleware);

    let address = format!("{}:{}", gateway_clone, listening_port);

    info!("Starting HTTP server on {}", &address);

    if let Err(e) = Iron::new(chain).http(&address) {
        error!("Failed to start HTTP Server: {:?}", &e);
        exit(
            &exit_tx_clone,
            ErrorKind::StartHTTPServer(address, e.to_string()).into(),
        );
    } else {
        info!("HTTP server started on {}!", &address);
    }
}

fn networks(req: &mut Request) -> IronResult<Response> {
    info!("User connected to the captive portal");

    let request_state = get_request_state!(req);

    if let Err(e) = request_state.network_tx.send(NetworkCommand::Activate) {
        return exit_with_error(&request_state, e, ErrorKind::SendNetworkCommandActivate);
    }

    let networks = match request_state.server_rx.recv() {
        Ok(result) => match result {
            NetworkCommandResponse::Networks(networks) => networks,
        },
        Err(e) => return exit_with_error(&request_state, e, ErrorKind::RecvAccessPointSSIDs),
    };

    let access_points_json = match serde_json::to_string(&networks) {
        Ok(json) => json,
        Err(e) => return exit_with_error(&request_state, e, ErrorKind::SerializeAccessPointSSIDs),
    };

    Ok(Response::with((status::Ok, access_points_json)))
}

fn connect(req: &mut Request) -> IronResult<Response> {
    info!("Received connection request");
    let (ssid, credentials) = {
        let uploads_directory = PathBuf::from(
            env::var("UPLOADS_DIRECTORY").unwrap_or_else(|_| DEFAULT_UPLOADS_DIRECTORY.to_string()),
        );
        let params = get_request_ref!(req, Params, "Getting request params failed");
        let ssid = get_param!(params, "ssid", String);
        let security = get_param!(params, "security", String);
        let wifi_authentication = get_param!(params, "wifi-authentication", String);

        let credentials = match (security.as_str(), wifi_authentication.as_str()) {
            // FIXME: not supported yet
            // ("enterprise", "fast") => {
            //     let anonymous_identity = get_param!(params, "anonymous-identity", String);
            //     let identity = get_param!(params, "identity", String);
            //     let password = get_param!(params, "password", String);
            //     let phase1_fast_provisioning = get_param!(params, "phase1-fast-provisioning", bool); //FIXME: casting might be broken here
            //     let phase2_auth = get_param!(params, "phase2-auth", String);

            //     AccessPointCredentials::EnterpriseFAST {
            //         anonymous_identity,
            //         identity,
            //         password,
            //         phase1_fast_provisioning,
            //         phase2_auth,
            //     }
            // }
            ("enterprise", "leap") => {
                let identity = get_param!(params, "identity", String);
                let password = get_param!(params, "password", String);

                AccessPointCredentials::EnterpriseLEAP { identity, password }
            }
            ("enterprise", "peap") => {
                let anonymous_identity = get_optional_param!(params, "anonymous-identity", String);

                let mut ca_cert: Option<String> = None;
                get_optional_upload!(
                    params,
                    "ca-cert",
                    &uploads_directory,
                    "certs",
                    "pem",
                    ca_cert
                );

                let domain_suffix_match = get_param!(params, "domain-suffix-match", String);
                let identity = get_param!(params, "identity", String);
                let password = get_param!(params, "password", String);
                let phase2_auth = get_optional_param!(params, "phase2-auth", String);
                let phase2_auth_eap = get_optional_param!(params, "phase2-autheap", String);

                AccessPointCredentials::EnterprisePEAP {
                    anonymous_identity,
                    ca_cert,
                    domain_suffix_match,
                    identity,
                    password,
                    phase2_auth,
                    phase2_auth_eap,
                }
            }
            ("enterprise", "pwd") => {
                let identity = get_param!(params, "identity", String);
                let password = get_param!(params, "password", String);

                AccessPointCredentials::EnterprisePWD { identity, password }
            }
            ("enterprise", "tls") => {
                let ca_cert: String;
                get_upload!(
                    params,
                    "ca-cert",
                    uploads_directory,
                    "certs",
                    "pem",
                    ca_cert
                );

                let client_cert: String;
                get_upload!(
                    params,
                    "client-cert",
                    uploads_directory,
                    "certs",
                    "pem",
                    client_cert
                );

                let domain_suffix_match = get_param!(params, "domain-suffix-match", String);
                let identity = get_param!(params, "identity", String);

                let private_key: String;
                get_upload!(
                    params,
                    "private-key",
                    &uploads_directory,
                    "keys",
                    "pem",
                    private_key
                );

                let private_key_password = get_param!(params, "private-key-password", String);

                AccessPointCredentials::EnterpriseTLS {
                    ca_cert,
                    client_cert,
                    domain_suffix_match,
                    identity,
                    private_key,
                    private_key_password,
                }
            }
            ("enterprise", "ttls") => {
                let anonymous_identity = get_param!(params, "anonymous-identity", String);
                let ca_cert: String;
                get_upload!(
                    params,
                    "ca-cert",
                    &uploads_directory,
                    "certs",
                    "pem",
                    ca_cert
                );
                let domain_suffix_match = get_param!(params, "domain-suffix-match", String);
                let identity = get_param!(params, "identity", String);
                let password = get_param!(params, "password", String);
                let phase2_auth_eap = get_param!(params, "phase2-autheap", String);

                AccessPointCredentials::EnterpriseTTLS {
                    anonymous_identity,
                    ca_cert,
                    domain_suffix_match,
                    identity,
                    password,
                    phase2_auth_eap,
                }
            }
            ("wpa", _) => {
                let passphrase = get_param!(params, "password", String);

                AccessPointCredentials::Wpa { passphrase }
            }
            ("wep", _) => {
                let passphrase = get_param!(params, "password", String);

                AccessPointCredentials::Wep { passphrase }
            }
            (_, _) => AccessPointCredentials::None,
        };
        (ssid, credentials)
    };

    debug!("Incoming `connect` to access point `{}` request", ssid);

    let request_state = get_request_state!(req);

    info!("Credentials: {:?}", &credentials);

    let command = NetworkCommand::Connect { ssid, credentials };

    if let Err(e) = request_state.network_tx.send(command) {
        exit_with_error(&request_state, e, ErrorKind::SendNetworkCommandConnect)
    } else {
        Ok(Response::with(status::Ok))
    }
}
