export interface NetworkInfo {
	ssid: string;
	security: string;
	'wifi-authentication'?: string;
	'anonymous-identity'?: string;
	'ca-cert'?: string;
	'client-cert'?: string;
	'domain-suffix-match'?: string;
	identity?: string;
	password?: string;
	'phase2-auth'?: string;
	'phase2-autheap'?: string;
	'private-key'?: string;
	'private-key-password'?: string;
}

export interface Network {
	ssid: string;
	security: string;
}
