import * as React from 'react';

import styled from 'styled-components';

const StyledHeader = styled.div`
	background-color: var(--primary-bg-color, var(--default-primary-bg-color));
	padding: 16px;
	max-height: 64px;
	flex-shrink: 0;
`;

const BrandBox = styled.div`
	background-image: var(--brand-image, var(--default-brand-image));
	background-repeat: no-repeat;
	background-size: contain;
	background-position: center;
	height: 32px;
`;

export const Header = () => (
	<StyledHeader>
		<BrandBox />
	</StyledHeader>
);
