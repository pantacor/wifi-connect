import React from 'react';
import { Provider, Container, Txt, Heading } from 'rendition';
import { NetworkInfoForm } from './NetworkInfoForm';
import { Footer } from './Footer';
import styled, { createGlobalStyle } from 'styled-components';
import { Network, NetworkInfo } from '../types';
import { NetworkList } from './NetworkList';
import { Header } from './Header';

const GlobalStyle = createGlobalStyle`
	:root {
    --default-primary-bg-color: #221f48;
    --default-secondary-bg-color: #221f48;
		--success-color: #28a745;
		--info-color: #17a2b8;
		--warning-color: #ffc107;
		--danger-color: #dc3545;
	--default-border-radius: 0;
    --default-light-color: #f8f9fa;
    --default-secondary-color: var(--default-light-color);
    --default-brand-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYYAAABYCAYAAADxw1AOAAAP3UlEQVR4nO2dP6gkWRWH20RYEX3ZwgZaICsuij4j2Wg6UBGD3TEyEacVRCNnMJdXhhvNBKbuNIJusjATqix2g4lg8J6hUT9BUaP3AsVE+Axu9U5Pv1t1zv1TdauqzwcFw/Duvefcqr6/+/fcxWLkAEvgKbDBcdP8+zFwXto+wzAMYyCA8wMx6OIpcFbaXsMwDKNHGlG4UYjCnkugKm23YRiG0QMRonAoDjZyMAzDmBvopo/aqEvbbxiGYWSkGS2kYqMGwzCMuQA8ySAMq9J+GIZhGJkAthmEoS7th2EYhpEJ3AJyKtvSfhiGYRiZIM+I4XlpPwzDME4a3ILxQ+Di4LkXmVexqSTg7SMfHmJnIwzDMPTgQlV0Tf3sgAeBeT7KIAz3A8u8oPvcxAZYBlWOYRjGqYGLU6TlGcotpEAF3EYKAsB1gA9nhK1pXERXmGEYxpwhbkvpJiD/OiL/PStlGaGisOdRdMUZhmHMEeB+QqNdB5RzFZH/OiD/lPMSy4iqMwzDmCe4dYNYbtBPKZ0RthC9DvChSvABAkY/hmEYs4Y84SpWgWWu6F5zuCZ8sTllqmpPFVKmYRjGLMnUoEadMcBNYdW4KaC6eaIu6SHPllhbazAMwyCPMGxH4Mcugx91aT8MwzCKA6wzNKjbEfiRsh12T13aD8MwjOIwnxGDBeozDMPIAWlbVcUGFXeS+qJ5gtcPGvseS+nJE9o7an1jaIB7Qz6l/Z0zuJ16XfVflbbROFFIn4bxNqj4Q2GsAuzyncT2pscJUArXUZU3ELgG5GmijynscO9jEuI5FZC/27q0jcaJQtp00rolz7OONCuFTauWtDcdaVKmk0SbSkH8fdl98Ri7US8LmDAYY4a4U8m3bQ2E4oNvPadAuyjsaRuhnBM3+hl1SG/S7svui0tMHJLBhMEYM7gefog43NI95y998De+9MiiAB3hKwgXhy0jbuDIcwCxL+y0eCKYMBhjBycOmkXcNUJjqvjg91zgGr97zb81dM5z48JjSNNKt0zgR0eekOV9YocCE8CEwZgKuIb1Ea5xvWqeLW4tolLmkboY3EqAH+c4odviwmxsgeeNb6MdJRxCnu3EfbIrXUdTBhMG45SgxymQ0r4NCfoF9dvmb1OfmLWmVel6miqYMBinBN27kpIo7duQoBOGKzKOgJp3JwU6PGSdq+xTAxMG45RQfPApnMxeenTCEBSJNqBs7UK+TSdFggmDcUrQ7xbLZ6X9Gwp0wrDssXzNLrGTGsXlBBMGY07w8rH986P/32gak0SecRCugRc7m+4xozAOlBcG7ZTgHRuO30mGp+rLz1LQszDgNpLkfAcnM1rfw8thSx7wIsTP4XOy9bNYLPQ9yBFwwwwWRSksDCk2KNPFsMOFB3kQ6c9bwLvAfxNs+AfwZ+B3wC+Bd4C3ImzJNeW6bcm/z11tG1yjWMW8hxZ7K+AhruO3aZ7UU//LQBvOGxtSyr6k+UbpYQck8AbwA9yuy9/ivsW/IXzTue3YG9PnukFfTFrBGYcwPFfYcGedQ2l7Kjtf2R2+/KJne/4D/Bz4pNKeKQvDIU+JbABxPfIL8typ4mOptOMBrkHPzQ2ufqqY+jmw7yPAd4A/xhrSlvE5Ls7N5uB5hlLVyBPBdGhqhV++Xsqm+b8q+A1mhHEIg6ZxqSNtz8WFwo/vDWjPP4GvKWyaizBARJgUXPTkXc92LQUbKoYLOyN+py02vgr8PrXwGMdvJKOZoTDgeirScPEphQ7EYcIQQucpbOAPA9sD8G3BpjkJA8BlwHc11LT0ssOGEgEqn2rrqLHxdVxHI5kUxze0B8bLcXfD0LQF3DsjbNh42ZZXn2DCEMINLSM84JWBbTnk9Y66nZswgG6UPuRa5bLFhpJRi1XigBsp/DVXoamOt24HZVqjhlWHHzFziTsGHjkwDmHQrDGsPOn24UhSntCT2HWLD58KzCcn73fUbd/CsCLPifiQwJStofEbm6osHutZttgR2gZc4b7p2vOE1hEo4owB7wfm2ck+001CHqsOY4dU+1i67E/pRa2ll5kTxiEMY7ChQtcp8R62ww3HJR4AnxDseBV4E/g+8J4izz2faclPEoY6Q/VlA/2J+K6w+htlnV3h4pote/BD2wZcEXa52BJdRwpcp721owl8UZkPwAfAj4FvAl8APt5lYAqdp1kZrzjcIrxI0oePleIbyQLjaJQ19dWrDQe2aL67ypNOEobfRNrzVeBfCpt+0pJ+UsKwWHw4DSuJw5OWtJoYarf0dJr/wA7NN+31QZn/fUUdQfe1yJoddP8GvhFi2FqRqYQU3nqM4iDZnGOdJPqDCYXyB9y09TXYFBvy9NLSk0YShl8l2PNlRf180JJ2csKwWCwWyOHgty3pNKO+Xtfy0LVbdYZyNJ3z1g447lyCxFdCjcqxH1esHMYVFlozZ5fD3m3Qy0iA8sKg+Y5u+yq/xSbpHd75DuhRGJr83xXy/3tLuqkKg2T3tiXdrrS/yFM912Tq6BAphOjWYdYxBuWgVpSTa/EsB2JPA/38XxeDBY2jkDDgPkxt52LQq1GRe6u1J81nhTS/TrTpTamSWtJNVRikUCl3FqDRTSP1PvJEnkbKdvkUuga+9qT7uiLd52MMysEdgz3lTE0YckyxXQe/kEjQCcND8sXC2R/0C2HQW9yIaEyRheG9RJs+CvxPKOO1HL6MBcHuO0KIPC25HcBmTUOdVZyQO6N3OlbAt4Q0f4k1JubClWNqRTk2ldQjDH8WIJRbht/CGyMMvU4lNWXshDI+l8OXsSDY7RMG6bdXD2CzVN9XPZQp+X1nBgL4rpCmdQu0ZIwtPvttzrH4XEe9lAgYvzCsFT6c4baCHocciX2kKa7aY0OvI4amjD8JZbzhSTOYMOBGhMchcVKeTjzlS23SKpevHXUgNdLZp0VRtDmeND8UkrwTa0zqFM+1kP8YRQEUUVVxi0spVFEvJQLGLQziaAE3NTX06dLaY8cQwiDFsikiDLi5/T6Cw3XisUP6lpepvirqYvBRC4q22JPmR0KSn6UYlNKodB1QGasoHLLqyf519AuJgPEKwy3y6OxpIdtqjy0nKQwUDPvgseUkhaEpV2J59PeSMPw0xRjNQRQf64485xISI2Z3Uta7lTUwTmG4Ro5YWbLzUHvsOTlhwC20looFZMLwcrkSy6O/l4QhKkrrYQHnhE2drDvyGtMOJC3LFl/OCBOHK8YbRG8ornE/LGn66IyCDRImDPu8c6wzRuOxx4RB6Tt9C0NTyBnyR3KNPDc/pdHCnlrw6RGK4/2MO+z2FXkCpfmeunnUokj5qcbaY9NJCQPDB6q7g8cmEwal7wwhDAeFVbiGcM3LP3xVXBJmKAyNX2e4nQOH0UDXuAauiCAc2KYRhmVJG49R2nyF+/aWEU8fB9zmJgxSHYHrENW4bz/mPXTiscmEoZ3zo78fThhSYZpTSVXpekuBaQqDRNKWQEZ4wK0pY0zCIE2TipsHFGV04vn7kxQG+tmVNB5hWCxGMU2gRYyyOgWYmDCgC3uQ2iCZMMi27PrI96iMTjx/L4nVKtUmhc1Rwf8Sy4w5xzAtYdjDy0PK86P/1zRmqTznoEHENUgf2lSiTvpAWZfL0nbuQW7okgPuKcqoPWlOTRgklhn87cTz90WmcY5skOo7exw0hd/XnjTTFAYJ+hWHQYO2lURZj8vSdu4hMupm5jJqT5qpCsM20haJKoO/nXj+fiqxkqrMZW6E8nyxkmYrDH2uRwy+bbQUmDDElFF70kxVGKJ6sEKekGFThVRAhK+dN5rlAnkX4hiiq85WGKSwvNGU9m1ImJ8wJA/VFWXUnjRTFQaI6MEq8lxm8LeTljSDNcoddq8FG3aUv49htsKgWYSMorRvQ8L8hAESf3SKMmpPmrEKg6ZH+SzCFqkBTr46UzK6JY3UKN8wjhvcnmYoR/NbuG5JOz5haBx6zMuBty5xsW+WAXn0QoAfDxqbdwfJN41vVWz9DAnTEwbNSHGVWIb0w649aUYpDE26a0WdBTVUyN/NJoO/nbSk0UQ33tH/PeaaUEHR4tD4GX2vNGMSBtyPeqNwZoMcFkErDDUvdhRJq/d7pOBt58jb9cCJRtEDbBJMTBgWC1VDd0NCjxX5G609acYsDFIves8OF61WcyGVZgrjcYKv4gG6jrQaIQT3+3w71kbBfm1bswMeBOS7RH/RVWt0YsYiDIRHYewc8qHYtuhLj26Yt+wodxXoxyUjFgemKQzahu6G8DsAdop8a49NYxaGlNH1pnmeHOUZcu/IJWHvQBPCu/XCG+L8jflWPnw8NpwRfpnZJW624cLzbAiPD1Z31FF5YcBV0i7QKehYpEF++Slhvb2CRHyI4ctcdZkbpikMvU0jKrnzbTFiYWjSasW0ja0nz5jIyrm4Y09mf4NoseGccnUk1c8ohCHlJa1b8uyaa14pbGoTh9YDUuimwdoY9N5iLUxQGBaL3s+xSFQee8YuDDE92EO2njw18ZL6wjt3fmTfYOLQYUMJcRDD91NaGMgThbFqydvXuK8CbPPNk3rTk35l501UBfYM0xWGijK9sW2LPaMWhiZ9iji0+T1oz/yASllngwTmFGw4J02UQxAFs7GpuDDk6FW09rZ5saj8hIgGrEn/BCH0M3l+AMH29Q0TFYbFYvAfHDghqlpsGb0wHOSjCQV/zLYjv6GjIgeNvnHfSa8jTKUdK9Kv/W3jOWGh64sLQ44GtXiICvJ8WHVpP45R+rUsbWcXuB9czI15Wm5x33Hr8JwJCcNBfoeh4CW2Ql7Lpo76avjAveOU3WbnOFHM/q1E1ntKp+YW58cj4g4oFheGHA3qtlcjdX7koC7txzEcBQdseUa7q8qH0ifVE1DmK0JeQY12SxlfEsr4WGoZTTm++gs6FIab6svyDvr8/nDTa4N9Jy027PN5xIuLq3zPqvm7KoPfrwk+fTq1DMmAuQhDjp5QXdoPwzCM4qA/6NGFakHFU/bbuD3Ah3uDo47Dk0fgVjFlG4ZhzArSd/NA4Lwi7vRm13mDHQGnDZs8cyyiV0GVZxiGMVdIm4a5DihHG3JjjzpWCenbI7cxdWcYhjFLSBs1qEcL6I7SHxMiDinTYlVU5RmGYcwV4vY6rwPyT2m0VwHlxKw1qPM3DMM4KQgTB/WCM+mnq9WXu+Cmq9YBea+iKsswDONUwO2R7ep1bwncF0yeheHQBe4V3Wsna2z6yDAMQw+ul7/i5cMbVWReObaS1pFlnx/5cZ+JHQgzDMOYHZmEoXjYDcMwDCMT5Amkti3th2EYhpEJZhrgzjAMw4iEPGF/V6X9MAzDMDKBWwBOxRaMDcMw5gRp00l1afsNwzCMzBAfy0i8H9UwDMOYKIRfvH2FHUIzDMOYN+jvd13bSMEwDOOE4MUdtHuRuG3+/YTIy3oMwzCMdv4PACoHHnLdM3wAAAAASUVORK5CYII=');
    --default-footer-bg-color: #273f52;
    --default-footer-color: var(--default-light-color);
	}

	body {
    margin: 0;
    background-color: var(--light-color);
		font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
			'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
			sans-serif;
		-webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    min-height: 100vh;
  }
  
  #root {
    min-height: 100vh;
    display: flex;
    flex-direction: column;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace;
  }
  
  .wifi-connect-button {
    background-color: var(--secondary-bg-color, var(--default-secondary-bg-color)) !important;
    border: none;
    border-radius: var(--primary-button-border-radius, var(--border-radius, var(--default-border-radius)));
    color: var(--secondary-color, var(--default-secondary-color)) !important;
  }

  .visually-hidden {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
  }
`;

const StyledProvider = styled(Provider)`
	display: flex;
	flex-direction: column;
	min-height: 100vh;
`;

const StyledContainer = styled(Container)`
	flex: 1;
	padding: 1rem 1rem 0.5rem;
	min-width: 340px;
	h3 {
		font-size: 1.375rem;
		padding: 0 1rem;
	}
`;

interface RefreshNetworksParameters {
	setAvailableNetworks: (network: Network[]) => void;
	setIsFetchingNetworks: (is: boolean) => void;
	setError: (error: string) => void;
}

const refreshNetworks = ({
	setAvailableNetworks,
	setError,
	setIsFetchingNetworks,
}: RefreshNetworksParameters) => {
	console.log('refreshing networks');
	setIsFetchingNetworks(true);
	// For testing:
	//   setAvailableNetworks([
	//     {
	//       ssid: "wpa",
	//       security: "wpa",
	//     },
	//     {
	//       ssid: "wep",
	//       security: "wep",
	//     },
	//     {
	//       ssid: "none",
	//       security: "",
	//     },
	//     {
	//       ssid: "enterprise",
	//       security: "enterprise",
	//     },
	//   ]);
	//   setIsFetchingNetworks(false);
	//   return Promise.resolve();
	return fetch('/networks')
		.then((data) => {
			if (data.status !== 200) {
				throw new Error(data.statusText);
			}

			return data.json();
		})
		.then(setAvailableNetworks)
		.catch((e: Error) => {
			setError(`Failed to fetch available networks.`);
			console.error(e);
		})
		.finally(() => {
			setIsFetchingNetworks(false);
		});
};

const App = () => {
	const [attemptedConnect, setAttemptedConnect] = React.useState(false);
	const [isFetchingNetworks, setIsFetchingNetworks] = React.useState(true);
	const [error, setError] = React.useState('');
	const [availableNetworks, setAvailableNetworks] = React.useState<Network[]>(
		[],
	);
	const [ssid, setSSID] = React.useState('');

	const chosenNetwork = availableNetworks.find(
		(network) => network.ssid === ssid,
	);

	React.useEffect(() => {
		// initial networks load
		refreshNetworks({ setAvailableNetworks, setError, setIsFetchingNetworks });
	}, []);

	const onConnect = (data: NetworkInfo) => {
		setAttemptedConnect(true);
		setError('');

		// handle mschapv2 with EAP special case
		if (data['phase2-auth'] === 'mschapv2-eap') {
			data['phase2-autheap'] = 'mschapv2';
			delete data['phase2-auth'];
		}

		fetch('/connect', {
			method: 'POST',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		})
			.then((resp) => {
				if (resp.status !== 200) {
					throw new Error(resp.statusText);
				}
			})
			.catch((e: Error) => {
				setError(`Failed to connect to the network.`);
			});
	};

	return (
		<StyledProvider>
			<GlobalStyle />
			<Header />

			<StyledContainer>
				{error === '' ? (
					!attemptedConnect ? (
						chosenNetwork === undefined ? (
							<NetworkList
								availableNetworks={availableNetworks}
								isFetchingNetworks={isFetchingNetworks}
								onSetSSID={(newSSID) => setSSID(newSSID)}
								onRefresh={() =>
									refreshNetworks({
										setAvailableNetworks,
										setError,
										setIsFetchingNetworks,
									})
								}
							/>
						) : (
							<NetworkInfoForm network={chosenNetwork} onSubmit={onConnect} />
						)
					) : (
						[
							<Heading.h3>Applying changes...</Heading.h3>,
							<Container p={2}>
								<Txt.p>Your device will soon be online.</Txt.p>
								<Txt.p>
									If the connection is unsuccessful, the Access Point will be
									back up in a few minutes, and reloading this page will allow
									you to try again.
								</Txt.p>
								<Txt.p className="attempted-connect-extra-text">
									<Txt.span>Thank you!</Txt.span>
								</Txt.p>
							</Container>,
						]
					)
				) : (
					[
						<Heading.h3>There was an error</Heading.h3>,
						<Container p={2}>
							<Txt.p>
								Please reload this page in a few minutes and try again.
							</Txt.p>
							<Txt.p>
								Error: <code>{error}</code>
							</Txt.p>
						</Container>,
					]
				)}
			</StyledContainer>
			<Footer />
		</StyledProvider>
	);
};

export default App;
