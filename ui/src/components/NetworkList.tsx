import * as React from 'react';

import { Flex, Heading, Txt, Spinner } from 'rendition';
import styled from 'styled-components';

import { Network } from '../types';

import { NetworkIcon } from './NetworkIcon';

interface NetworkListProps {
	availableNetworks: Network[];
	isFetchingNetworks: boolean;
	onSetSSID: (ssid: string) => void;
	onRefresh: () => void;
}

interface NetworkSecurityIconProps {
	security: string;
}

const InnerNetworkListItem = styled(Flex)`
	border-bottom: 1px var(--footer-bg-color, var(--default-footer-bg-color))
		solid;

	&:last-child {
		border-bottom: none;
	}
`;

const NetworkSSID = styled(Txt)`
	cursor: pointer;
	font-size: 1.5em;
`;

export const NetworkList = ({
	availableNetworks,
	isFetchingNetworks,
	onSetSSID,
	onRefresh,
}: NetworkListProps) => (
	<Flex
		flexDirection="column"
		alignItems="center"
		justifyContent="center"
		mb={3}
	>
		<Heading.h3 align="left" mb={2}>
			{isFetchingNetworks ? (
				<Txt.span>Getting available networks...</Txt.span>
			) : availableNetworks.length > 0 ? (
				<Txt.span className="network-list-heading-text">
					<Txt.span>Hi! Please choose your WiFi from the list below.</Txt.span>
				</Txt.span>
			) : (
				<Txt.span>No wireless networks found</Txt.span>
			)}
		</Heading.h3>
		{isFetchingNetworks ? (
			<Flex alignItems="center" mt={2}>
				<Spinner emphasized show />
			</Flex>
		) : availableNetworks.length > 0 ? (
			<Flex
				flexDirection="column"
				alignItems="start"
				justifyContent="start"
				width="100%"
			>
				{availableNetworks.map((net) => (
					<InnerNetworkListItem
						justifyContent="space-between"
						alignItems="center"
						onClick={() => onSetSSID(net.ssid)}
						padding={3}
						width="100%"
					>
						<NetworkSSID>{net.ssid}</NetworkSSID>
						<NetworkIcon network={net} />
					</InnerNetworkListItem>
				))}
			</Flex>
		) : (
			<Txt>
				Please ensure there is a network within range and refresh or reboot the
				device.
			</Txt>
		)}
		{/*<Button
			className={'wifi-connect-button'}
			width="60%"
			mx="20%"
			mt={3}
			mb={4}
			disabled={isFetchingNetworks}
			onClick={() => onRefresh()}
		>
			Refresh Networks
		</Button>*/}
	</Flex>
);
