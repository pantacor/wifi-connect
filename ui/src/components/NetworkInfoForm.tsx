import { JSONSchema7 as JSONSchema } from 'json-schema';
import * as React from 'react';
import { Flex, Form, Heading, RenditionUiSchema } from 'rendition';
import { Network, NetworkInfo } from '../types';

const getSchema = (
	isEnterprise: boolean,
	networkInfo: NetworkInfo,
): JSONSchema => ({
	type: 'object',
	properties: {
		'wifi-authentication': {
			title: 'Authentication',
			type: 'string',
			default: '',
			oneOf: [
				{
					const: '',
					title: 'None',
				},
				{
					const: 'leap',
					title: 'LEAP',
				},
				{
					const: 'peap',
					title: 'Protected EAP (PEAP)',
				},
				{
					const: 'pwd',
					title: 'PWD',
				},
				{
					const: 'tls',
					title: 'TLS',
				},
				{
					const: 'ttls',
					title: 'Tunneled TLS',
				},
			],
		},
		'anonymous-identity': {
			title: 'Anonymous Identity',
			type: 'string',
		},
		'ca-cert': {
			title: 'CA Certificate',
			type: 'string',
		},
		'client-cert': {
			title: 'Client Certificate',
			type: 'string',
		},
		'domain-suffix-match': {
			title: 'Domain',
			type: 'string',
			default: '',
		},
		identity: {
			title: 'User',
			type: 'string',
			default: '',
		},
		password: {
			title: 'Password',
			type: 'string',
			default: '',
		},
		// FIXME: limit phase2-auth to those useable by the selected auth
		// FIXME: MSCHAP v2 with EAP should go to the `phase2-autheap` property instead
		'phase2-auth': {
			title: 'Inner Authentication',
			type: 'string',
			oneOf: [
				{
					const: 'chap',
					title: 'CHAP',
				},
				{
					const: 'eap',
					title: 'EAP',
				},
				{
					const: 'gtc',
					title: 'GTC',
				},
				{
					const: 'mschap',
					title: 'MSCHAP',
				},
				{
					const: 'mschapv2',
					title: 'MSCHAP v2 (No EAP)',
				},
				{
					const: 'mschapv2-eap',
					title: 'MSCHAP v2 (EAP)',
				},
			],
		},
		'private-key': {
			title: 'User Private Key',
			type: 'string',
		},
		'private-key-password': {
			title: 'User Key Password',
			type: 'string',
		},
	},
	required: ['password'].concat(
		isEnterprise
			? ['identity'].concat(
					networkInfo['wifi-authentication'] === 'peap' ? ['phase2-auth'] : [],
			  )
			: [],
	),
});

const getUiSchema = (
	isEnterprise: boolean,
	networkInfo: NetworkInfo,
): RenditionUiSchema => {
	const hasSSID = networkInfo.ssid !== undefined;

	const wifiAuth = networkInfo['wifi-authentication'] || '';

	return {
		'wifi-authentication': {
			'ui:widget': !(hasSSID && isEnterprise) ? 'hidden' : undefined,
			'ui:options': {
				emphasized: true,
			},
		},
		'anonymous-identity': {
			'ui:widget':
				['fast', 'peap', 'ttls'].includes(wifiAuth) && isEnterprise
					? 'password'
					: 'hidden',
		},
		'ca-cert': {
			'ui:widget': !(['peap', 'tls', 'ttls'].includes(wifiAuth) && isEnterprise)
				? 'hidden'
				: 'file',
		},
		'client-cert': {
			'ui:widget': wifiAuth !== 'tls' || !isEnterprise ? 'hidden' : 'file',
		},
		'domain-suffix-match': {
			'ui:widget': !(['peap', 'tls', 'ttls'].includes(wifiAuth) && isEnterprise)
				? 'hidden'
				: undefined,
		},
		identity: {
			'ui:options': {
				emphasized: true,
			},
			'ui:widget':
				!isEnterprise || wifiAuth === 'not-set' ? 'hidden' : undefined,
		},
		password: {
			'ui:widget': hasSSID ? 'password' : 'hidden',
			'ui:options': {
				emphasized: true,
			},
		},
		'phase2-auth': {
			'ui:widget': !(['peap', 'ttls'].includes(wifiAuth) && isEnterprise)
				? 'hidden'
				: undefined,
		},
		'private-key': {
			'ui:widget': wifiAuth !== 'tls' || !isEnterprise ? 'hidden' : 'file',
		},
		'private-key-password': {
			'ui:widget': wifiAuth !== 'tls' || !isEnterprise ? 'hidden' : 'password',
		},
	};
};

const validateNetworkInfo = (network: Network, networkInfo: NetworkInfo) => {
	if (
		network.security === 'enterprise' &&
		networkInfo['wifi-authentication'] === ''
	) {
		return false;
	}

	if (
		network.security === 'enterprise' &&
		(networkInfo.identity === '' || networkInfo.password === '')
	) {
		return false;
	}

	if (
		network.security === 'enterprise' &&
		networkInfo['wifi-authentication'] === 'peap'
	) {
		if (
			[
				networkInfo.identity,
				networkInfo.password,
				networkInfo['phase2-auth'],
			].every((v) => v === '' && v === undefined)
		) {
			return false;
		}
	}

	if (network.security === 'wpa' && networkInfo.password === '') {
		return false;
	}

	return true;
};

interface NetworkInfoFormProps {
	network: Network;
	onSubmit: (data: NetworkInfo) => void;
}

export const NetworkInfoForm = ({
	network,
	onSubmit,
}: NetworkInfoFormProps) => {
	const { ssid, security } = network;

	const [networkInfo, setNetworkInfo] = React.useState<NetworkInfo>({
		ssid,
		security,
	});

	const isSelectedNetworkEnterprise = security === 'enterprise';

	const isValid = validateNetworkInfo(network, networkInfo);

	return (
		<Flex
			className="network-info-form"
			flexDirection="column"
			alignItems="center"
			justifyContent="center"
		>
			<Heading.h3 align="center">
				Setting up network:
				<br /> <code>{ssid}</code>
			</Heading.h3>

			<Form
				width={'100%'}
				onFormChange={({ formData }) => {
					setNetworkInfo({ ...formData, ssid, security });
				}}
				onFormSubmit={({ formData }) =>
					onSubmit({ ...formData, ssid, security })
				}
				value={networkInfo}
				schema={getSchema(isSelectedNetworkEnterprise, networkInfo)}
				uiSchema={getUiSchema(isSelectedNetworkEnterprise, networkInfo)}
				submitButtonProps={{
					className: 'wifi-connect-button',
					width: '60%',
					mx: '20%',
					mt: 3,
					disabled: !isValid,
				}}
				submitButtonText={'Connect'}
			/>
		</Flex>
	);
};
