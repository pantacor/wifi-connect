import * as React from 'react';

import { Network } from '../types';

import { Flex } from 'rendition';

import { ReactComponent as SafeIcon } from './network-icons/safe.svg';
import { ReactComponent as ShieldIcon } from './network-icons/shield.svg';
import { ReactComponent as WifiFull } from './network-icons/wifi-full.svg';
import styled from 'styled-components';
// import WifiMedium from "./network-icons/wifi-medium.svg";
// import WifiLow from "./network-icons/wifi-low.svg";
// import WifiNone from "./network-icons/wifi-none.svg";

interface NetworkIconProps {
	network: Network;
}

const NetworkIconFlex = styled(Flex)`
	max-height: 1.5em;
`;

const NetworkIconsElementWrap = styled.div`
	padding: 0.125em;
	& > svg {
		max-height: 1.375em;
	}
`;

export const NetworkIcon = ({ network }: NetworkIconProps) => {
	// TODO: implement signal levels
	return (
		<NetworkIconFlex flexDirection="row">
			{['wep', 'wpa'].includes(network.security) ? (
				<NetworkIconsElementWrap>
					<SafeIcon />
				</NetworkIconsElementWrap>
			) : network.security === 'enterprise' ? (
				<NetworkIconsElementWrap>
					<ShieldIcon />
				</NetworkIconsElementWrap>
			) : null}
			<NetworkIconsElementWrap>
				<WifiFull />
			</NetworkIconsElementWrap>
		</NetworkIconFlex>
	);
};
